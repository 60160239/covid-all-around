import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'newscreate.dart';

class NewsWidget extends StatefulWidget {
  NewsWidget({Key? key}) : super(key: key);

  @override
  _NewsWidgetState createState() => _NewsWidgetState();
}

class _NewsWidgetState extends State<NewsWidget> {
  final _formKey = GlobalKey<FormState>();
  var newsHeadline = ['h1', 'h2'];
  var newsDate = ['d1', 'd2'];
  var newsContent = ['t1', 't2'];
  var newsQuantity = 0;
  var newsBoxDec = BoxDecoration(
      border: Border.all(width: 2, color: Colors.lightBlue),
      borderRadius: BorderRadius.circular(20));
  var hugeTextStyle = TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold);
  var normalTextStyle = TextStyle(fontSize: 16.0);
  String newsCreateHeadline = '';
  String newsCreateContent = '';

  @override
  void initState() {
    super.initState();
    print(newsHeadline);
    //_loadNews();
  }

  Future<void> _loadNews() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      print(newsHeadline.length);
      print(newsHeadline);
      var tempHeadline = prefs.getString('headline') ?? '-';
      var tempContent = prefs.getString('content') ?? '-';
      print(tempHeadline);
      for (var i = 0; i < newsHeadline.length; i++) {
        if (newsHeadline[i] != tempHeadline && tempHeadline != '-') {
          newsHeadline.add(tempHeadline);
          newsContent.add(tempContent);
          print('add');
          break;
        }
      }
      newsQuantity = newsHeadline.length;
      print(newsQuantity);
    });
  }

  void _saveNews(){
    for (var i = 0; i < newsHeadline.length; i++) {
        if (newsHeadline[i] != newsCreateHeadline && newsCreateHeadline != '-') {
          newsHeadline.add(newsCreateHeadline);
          newsContent.add(newsCreateContent);
          print('add');
          break;
        }
      }
  }

  Container newsCreate(String headline, String date, String content) {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: newsBoxDec,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${headline}',
            style: hugeTextStyle,
          ),
          Text('${date}'),
          SizedBox(
            height: 8,
          ),
          Text(
            '${content}',
            style: normalTextStyle,
          )
        ],
      ),
    );
  }

  ListView newsInit() {
    var children = <Widget>[];
    children.add(
      ElevatedButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  content: Stack(
                    children: <Widget>[
                      Positioned(
                        right: -40.0,
                        top: -40.0,
                        child: InkResponse(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: CircleAvatar(
                            child: Icon(Icons.close),
                            backgroundColor: Colors.red,
                          ),
                        ),
                      ),
                      Form(
                        key: _formKey,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: TextFormField(
                                autofocus: true,
                                //controller: _nameController,
                                validator: (value) {
                                  if (value == null ||
                                      value.isEmpty ||
                                      value.length < 5) {
                                    return 'กรุณาใส่หัวข้อข่าว';
                                  }
                                  return null;
                                },
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                decoration:
                                    InputDecoration(labelText: 'หัวข้อข่าว'),
                                onChanged: (value) {
                                  setState(() {
                                    newsCreateHeadline = value;
                                  });
                                },
                              ),
                            ),
                            Padding(
                                padding: EdgeInsets.all(8.0),
                                child: TextFormField(
                                  //controller: _ageController,
                                  validator: (value) {
                                    if (value == null ||
                                        value.isEmpty ||
                                        value.length < 5) {
                                      return 'กรุณาใส่เนื้อหาข่าว';
                                    }
                                    return null;
                                  },
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  decoration:
                                      InputDecoration(labelText: 'เนื้อหา'),
                                  onChanged: (value) {
                                    setState(() {
                                      newsCreateHeadline = value;
                                    });
                                  },
                                )),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: ElevatedButton(
                                  onPressed: () {
                                    if (_formKey.currentState!.validate()) {
                                      _saveNews();
                                      Navigator.pop(context);
                                    }
                                  },
                                  child: const Text('บันทึกข้อมูล')),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              });
        },
        child: Text("Open Popup"),
      ),
    );
    for (var i = 0; i < newsHeadline.length; i++) {
      children.add(SizedBox(
        height: 10,
      ));
      children.add(newsCreate(newsHeadline[i], newsDate[i], newsContent[i]));
    }
    return ListView(
      padding: EdgeInsets.all(16),
      children: children,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('News'),
        ),
        body: newsInit());
  }
}
