import 'package:flutter/material.dart';
import 'news.dart';
import 'profile.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'vaccine.dart';

void main() {
  runApp(MaterialApp(title: 'Covid 19 All Around', home: MyApp()));
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String name = '';
  String gender = 'M';
  String address = '';
  int age = 0;
  var vaccine = ['-', '-', '-'];
  var vaccineQuantity = 0;
  var questionScore = 0.0;
  var riskScore = 0.0;
  var hugeText = 20.0;
  var normalText = 16.0;
  var hugeTextStyle = TextStyle(
    fontSize: 20.0
  );
  var normalTextStyle = TextStyle(
    fontSize: 16.0
  );
  var normalBoxDec = BoxDecoration(
    border: Border.all(width: 2,color: Colors.lightBlue),
    borderRadius: BorderRadius.circular(20)
  );

  @override
  void initState(){
    super.initState();
    _loadProfile();
    _loadVaccine();
    //_loadNews();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name')??'-';
      gender = prefs.getString('gender')??'M';
      age = prefs.getInt('age')??0;
      address = prefs.getString('address')??'-';
    });
  }

  Future<void> _loadVaccine() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      vaccine = prefs.getStringList('vaccine')??['-','-','-'];
    });
    print(vaccine);
    vaccineQuantity = 0;
      for(var i=0;i<vaccine.length;i++){
        if(vaccine[i]!='-'){
          vaccineQuantity++;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Main Menu'),
        ),
        body: ListView(
          children: [
            Container(
              padding: EdgeInsets.all(20.0),
              child: Row(
                children: [
                  Expanded(
                      child: Container(
                        decoration: normalBoxDec,
                        padding: EdgeInsets.all(10),
                        child: Column(
                        children: [
                          Text('ข้อมูลผู้ใช้งาน',textAlign: TextAlign.center,style: hugeTextStyle,),
                          Padding(
                            padding: EdgeInsets.all(6.0),
                            child: Column(
                              children: [
                                Text('ชื่อ-นามสกุล'),
                                Text('${name}')
                              ],
                            )
                          ),
                          Padding(
                            padding: EdgeInsets.all(6.0),
                            child: Column(
                              children: [
                                Text('ที่อยู่อาศัย'),
                                Text('${address}')
                              ],
                            )
                          ),
                          Padding(
                            padding: EdgeInsets.all(6.0),
                            child: Column(
                              children: [
                                Text('จำนวนวัคซีนที่ได้รับ'),
                                Text('${vaccineQuantity}')
                              ],
                            )
                          ),

                        ],
                      )
                      ) 
                      ),
                      SizedBox(width: 20,),
                  Expanded(
                      child: Container(
                        decoration: normalBoxDec,
                        padding: EdgeInsets.all(10),
                        child: Column(
                        children: [
                          Text('ข้อมูลข่าวสาร',textAlign: TextAlign.center,style: hugeTextStyle,),
                          Padding(
                            padding: EdgeInsets.all(6.0),
                            child: Column(
                              children: [
                                Text('ข่าวใหม่ประจำวัน'),
                                Text('mock data')
                              ],
                            )
                          ),
                          Padding(
                            padding: EdgeInsets.all(6.0),
                            child: Column(
                              children: [
                                Text('ข่าวใหม่ประจำเดือน'),
                                Text('mock data')
                              ],
                            )
                          ),
                          Padding(
                            padding: EdgeInsets.all(6.0),
                            child: Column(
                              children: [
                                Text('ข่าวใหม่ประจำวันจากผู้พัฒนา'),
                                Text('mock data')
                              ],
                            )
                          ),

                        ],
                      )
                      )
                      ),
                ],
              ),
            ),
            Container(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(6),
                      child: Text('เมนูช่วยเหลือ',style: hugeTextStyle,),),
                    ListTile(
                      contentPadding: EdgeInsets.all(6),
                      title: Text('Profile'),
                      subtitle: Text('บันทึกข้อมูลที่จำเป็นของผู้ใช้งาน'),
                      onTap: () async {
                        await Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileWidget()));
                        await _loadProfile();
                      },
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.all(6),
                      title: Text('Vaccine'),
                      subtitle: Text('บันทึกและแก้ไขข้อมูลการได้รับการฉีดวัคซีนของผู้ใช้'),
                      onTap: () async {
                        await Navigator.push(context, MaterialPageRoute(builder: (context) => VaccineWidget()));
                        await _loadVaccine();
                      },
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.all(6),
                      title: Text('News'),
                      subtitle: Text('ข่าวสารเกี่ยวกับสถานการณ์ COVID-19'),
                      onTap: () async {
                        await Navigator.push(context, MaterialPageRoute(builder: (context) => NewsWidget()));
                        //await _loadProfile();
                      },
                    ),
                  ],
                ))
          ],
        ));
  }
}
