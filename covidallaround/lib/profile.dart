import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileWidget extends StatefulWidget {
  ProfileWidget({Key? key}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  final _formKey = GlobalKey<FormState>();
  String name = '';
  String gender = 'M';
  String address = '-';
  int age =0;

  final _nameController = TextEditingController();
  final _ageController = TextEditingController();


  @override
  void initState(){
    super.initState();
    _loadProfile();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name')??'-';
      _nameController.text = name;
      gender = prefs.getString('gender')??'M';
      age = prefs.getInt('age')??0;
      _ageController.text ='$age';
      address = prefs.getString('address')??'-';
    });
  }

  Future<void> _saveProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('name', name);
      prefs.setString('gender', gender);
      prefs.setInt('age', age);
      prefs.setString('address', address);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Profile'),),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16),
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                autofocus: true,
                controller: _nameController,
                validator: (value){
                  if(value==null||value.isEmpty||value.length<5){
                    return 'กรุณาใส่ข้อมูลชื่อ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'ชื่อ - นามสกุล'),
                onChanged: (value){
                  setState(() {
                    name = value;
                  });
                },
              ),),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                controller: _ageController,
                validator: (value){
                  var num = int.tryParse(value!);
                  if(num==null||num<=0){
                    return 'กรุณาใส่อายุ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'อายุ'),
                onChanged: (value){
                  setState(() {
                    age = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              )),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: DropdownButtonFormField(
                value:gender,
                items: [
                DropdownMenuItem(child: Row(children: [Icon(Icons.male),SizedBox(width: 8,),Text('ชาย')],),value: 'M',),
                DropdownMenuItem(child: Row(children: [Icon(Icons.female),SizedBox(width: 8,),Text('หญิง')],),value: 'F',),  
              ],
                onChanged: (String? newValue){
                  setState(() {
                    gender = newValue!;
                  });

                },)),
                Padding(padding: EdgeInsets.all(8.0)),
                Padding(
                padding: EdgeInsets.all(8.0),
                child: DropdownButtonFormField(
                value:address,
                items: [
                DropdownMenuItem(child: Text('-'),value: '-',),
                DropdownMenuItem(child: Text('ชลบุรี'),value: 'ชลบุรี',),
                DropdownMenuItem(child: Text('กรุงเทพ'),value: 'กรุงเทพ',),
                DropdownMenuItem(child: Text('ภูเก็ต'),value: 'ภูเก็ต',),
                DropdownMenuItem(child: Text('เชียงใหม่'),value: 'เชียงใหม่',),
                DropdownMenuItem(child: Text('สระแก้ว'),value: 'สระแก้ว',),
                DropdownMenuItem(child: Text('กำแพงเพชร'),value: 'กำแพงเพชร',),
                DropdownMenuItem(child: Text('สระบุรี'),value: 'สระบุรี',),
                DropdownMenuItem(child: Text('น่าน'),value: 'น่าน',),
                DropdownMenuItem(child: Text('เลย'),value: 'เลย',),
                DropdownMenuItem(child: Text('สุโขทัย'),value: 'สุโขทัย',),
              ],
                onChanged: (String? newValue){
                  setState(() {
                    address = newValue!;
                  });

                },)),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: ElevatedButton(
                onPressed: (){
                  if(_formKey.currentState!.validate()){
                    _saveProfile();
                    Navigator.pop(context);
                  }
                }, 
                child: const Text('บันทึกข้อมูล')),
              ),
            ],
          ),
        ),
      ),
    );
  }
}