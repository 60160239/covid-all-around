import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VaccineWidget extends StatefulWidget {
  VaccineWidget({Key? key}) : super(key: key);

  @override
  _VaccineWidgetState createState() => _VaccineWidgetState();
}

class _VaccineWidgetState extends State<VaccineWidget> {
  var vaccine = ['-','-','-'];
  var vaccineDate = ['-','-','-'];
  Widget _vaccineComboBox({required String title, required String value, ValueChanged<String?>? onChanged}){
    return Row(
            children: [
              Text(title),
              Expanded(child: Container()),
              DropdownButton(
                items: [
                  DropdownMenuItem(child: Text('-'), value: '-',),
                  DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer',),
                  DropdownMenuItem(child: Text('Johnson & Johnson'), value: 'Johnson & Johnson',),
                  DropdownMenuItem(child: Text('Sputnik'), value: 'Sputnik',),
                  DropdownMenuItem(child: Text('AstraZeneca'), value: 'AstraZeneca',),
                  DropdownMenuItem(child: Text('Novavax'), value: 'Novavax',),
                  DropdownMenuItem(child: Text('Sinopharm'), value: 'Sinopharm',),
                  DropdownMenuItem(child: Text('Sinovac'), value: 'Sinovac',),
                ],
                value: value,
                onChanged: onChanged,
              )
            ],
          );
  }

  Widget _vaccineDateInput(int i){
    return Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                validator: (value){
                  if(value==null||value.isEmpty||value.length<5){
                    return 'กรุณาใส่วันที่ได้รับการฉีดวัคซีน';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'วันที่/เดือน/ปี'),
                onChanged: (value){
                  setState(() {
                    vaccineDate[i] = value;
                  });
                },
              ),);
  }

  Future<void> _loadVaccine() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
        vaccine = prefs.getStringList('vaccine')??['-','-','-'];
        vaccineDate = prefs.getStringList('vaccine_date')??['-','-','-'];
    });
  }

  Future<void> _saveVaccine() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('vaccine', vaccine);
      prefs.setStringList('vaccine_date', vaccineDate);
    });
  }

  @override
  void initState() {
    super.initState();
    _loadVaccine();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Vaccine'),),
      body: Container(
        padding: EdgeInsets.all(16),
        child:ListView(
        children: [
          _vaccineComboBox(title: 'วัคซีนที่ได้รับเข็มที่ 1',value: vaccine[0],onChanged: (newValue) {
            setState(() {
              vaccine[0] = newValue!;
            });
          }),
          _vaccineDateInput(0),
          _vaccineComboBox(title: 'วัคซีนที่ได้รับเข็มที่ 2',value: vaccine[1],onChanged: (newValue) {
            setState(() {
              vaccine[1] = newValue!;
            });
          }),
          _vaccineDateInput(1),
          _vaccineComboBox(title: 'วัคซีนที่ได้รับเข็มที่ 3',value: vaccine[2],onChanged: (newValue) {
            setState(() {
              vaccine[2] = newValue!;
            });
          }),
          _vaccineDateInput(2),
          ElevatedButton(
            onPressed: (){
              _saveVaccine();
              Navigator.pop(context);
            },
            child: Text('Save'))
        ],
      )),
    );
  }
}