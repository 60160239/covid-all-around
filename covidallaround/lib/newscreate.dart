import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class NewsCreateWidget extends StatefulWidget {
  NewsCreateWidget({Key? key}) : super(key: key);

  @override
  _NewsCreateWidgetState createState() => _NewsCreateWidgetState();
}

class _NewsCreateWidgetState extends State<NewsCreateWidget> {
  final _formKey = GlobalKey<FormState>();
  String newsCreateHeadline = '';
  String newsCreateContent = '';

  Future<void> _saveNews() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('headline', newsCreateHeadline);
      prefs.setString('content', newsCreateContent);
      //prefs.setString('date', date);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Profile'),),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16),
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                autofocus: true,
                //controller: _nameController,
                validator: (value){
                  if(value==null||value.isEmpty||value.length<5){
                    return 'กรุณาใส่หัวข้อข่าว';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'หัวข้อข่าว'),
                onChanged: (value){
                  setState(() {
                    newsCreateHeadline= value;
                  });
                },
              ),),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                //controller: _ageController,
                validator: (value){
                  if(value==null||value.isEmpty||value.length<5){
                    return 'กรุณาใส่เนื้อหาข่าว';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'เนื้อหา'),
                onChanged: (value){
                  setState(() {
                    newsCreateHeadline = value;
                  });
                },
              )),
              
              Padding(
                padding: EdgeInsets.all(8.0),
                child: ElevatedButton(
                onPressed: (){
                  if(_formKey.currentState!.validate()){
                    _saveNews();
                    Navigator.pop(context);
                  }
                }, 
                child: const Text('บันทึกข้อมูล')),
              ),
            ],
          ),
        ),
      ),
    );
  }
}